package com.poc.csv.reader;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;

public class ReadCsv {

    //In this var you can put the path file,
    private String pathFile;
    CSVReader reader;
    
    //Array with all elements as string
    String[] labels;

    public ReadCsv(String pathFile) {
        this.pathFile = pathFile;
    }

    
    /**
     * This method open a csv file and process by line. 
     * In every line you have to read the line and call to the method distance
     * @return
     * @throws IOException
     */
    public List<Map<Double, String>> processFile() throws IOException {
        reader = new CSVReader(new FileReader(pathFile));
        List<?> content = reader.readAll();
        content.forEach(line -> {
            //HERE PROCESS THE INFORMATION BY LINE
            String[] row = (String[]) line;
            double distance = processLine(row);
            String label = row[5]+","+distance;
            //SO far you have an array with all elements but this array is not sorted.
            //perhaps the best option is sorted before insert with a hashMap and after
            //that convert to an sorted array.
            labels[labels.length] = label;
        });

        return null;
    }
    
    private double  processLine(String[] line){
        //get every element of the array
        //line[0]-->age
        //Example of binary element of maritalStatus
        int maritalStatus = line[1].equalsIgnoreCase("Married")?0:1;
        //line[5]-->car
        //call to distance private method.
        //return a double value.
    }
    
    /**
     * This method return a float value with the substract of all elements
     * @return a float value
     */
    private double distance(double num1,double num2,double num3, double num4){
        return (num1-num2)*(num3-num4);
    }
    
    private String[] arrayFrequent(String[]labels){
        //call to sortArray
        //iterate over k lines
        String[] labelsSorted = sortArray(labels);
        Arrays.stream(labelsSorted).forEach(label->{
            //DO SOMETHING
            //Process every element, save in a HashMap every element, after that convert
            //every Map(key,value) to an array.
        });
        
        //return array;
        
    }
    
    
    private String[] sortArray(String[]labels){
       //THis is example you have to take the distance, with this example i took the label (car+distance).
        Arrays.sort(labels, Comparator.comparing(String::length));
        return null;
    }

}
